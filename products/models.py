import uuid

from django.db import models

class Product(models.Model):
    sku = models.CharField(max_length=100, blank=True,
                           unique=True, default=uuid.uuid4)
    name = models.CharField(unique=True, blank=True, max_length=200)
    created_date = models.DateField(auto_now_add=True)
    updated_date = models.DateField(auto_now=True)
    parent = models.ForeignKey('self', blank=True,
                               null=True, related_name='children')

    def __unicode__(self):
        return '%s (%s)' % (self.name, self.sku)
