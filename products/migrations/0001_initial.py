# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sku', models.CharField(default=uuid.uuid4, unique=True, max_length=100, blank=True)),
                ('name', models.CharField(max_length=200)),
                ('created_date', models.DateField(auto_now_add=True)),
                ('updated_date', models.DateField(auto_now=True)),
                ('parent', models.ForeignKey(blank=True, to='products.Product', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
