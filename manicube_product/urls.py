from django.conf.urls import patterns, include, url
from django.contrib import admin

from products.views import ProductView, ProductDetailView

urlpatterns = patterns('',
    url(r'^api-auth/',
        include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/v1/products/$', ProductView.as_view()),
    url(r'^api/v1/products/(?P<pk>[0-9]+)',
        ProductDetailView.as_view()),
)
